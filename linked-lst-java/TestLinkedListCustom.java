public class TestLinkedListCustom {
    public static void main(String[] args) {
        LinkedListCustom lst = new LinkedListCustom();
        Node head = null;
        head = lst.insert(head, 1);
        head = lst.insert(head, 3);
        head = lst.insert(head, 2);
        head = lst.insert(head, 7);
        head = lst.insert(head, 5);
        head = lst.insert(head, 9);
        head = lst.insert(head, 1);
        lst.print(head);
        head = lst.delete(head, 3);
        head = lst.delete(head, 1);
        lst.print(head);
        System.out.println("contains(1) = " + lst.contains(head, 1));
        System.out.println("contains(7) = " + lst.contains(head, 7));
        head = lst.insertFirst(head, 11);
        lst.print(head);
        head = lst.insertAt(head, 13, 2);
        head = lst.insertAt(head, 77, 4);
        lst.print(head);
        head = lst.reverse(head);
        lst.print(head);
    }
}
