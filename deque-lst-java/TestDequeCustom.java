public class TestDequeCustom {
    public static void main(String[] args) {
        DequeCustom deque = new DequeCustom();
        deque.insert(1);
        deque.insert(2);
        deque.insert(7);
        deque.insert(4);
        deque.insert(3);
        deque.insert(6);
        deque.insert(8);
        deque.insertFirst(77);
        deque.insertFirst(11);
        deque.insert(123);
        deque.print();
        deque.reversePrint();
        //deque.removeFirst();
        deque.removeLast();
        deque.print();
        deque.reversePrint();
    }
}
