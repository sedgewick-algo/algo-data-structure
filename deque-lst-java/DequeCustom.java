public class DequeCustom {
    Node head;
    Node tail;

    public void insert(int val) {
        Node newNode = new Node(val);
        if(head == null) {
            head = tail = newNode;
        } else {
            tail.next = newNode;
            newNode.prev = tail;
            tail = newNode;
            tail.next = null;
        }
    }

    public void insertFirst(int val) {
        Node newNode = new Node(val);
        newNode.next = head;
        head.prev = newNode;
        head = newNode;
    }

    public void removeFirst() {
        head=head.next;
    }

    public void removeLast() {
        tail=tail.prev;
    }

    public void print() {
        Node tmp = head;
        while(tmp != null) {
            System.out.print(tmp.val + " ");
            tmp=tmp.next;
        }
        System.out.println();
    }

    public void reversePrint() {
        Node tmp = tail;
        while(tmp != null) {
            System.out.print(tmp.val + " ");
            tmp=tmp.prev;
        }
        System.out.println();
    }
}

class Node {
    int val;
    Node next;
    Node prev;

    public Node() {}

    public Node(int val) {
        this.val = val;
        this.next = this.prev = null;
    }
}
