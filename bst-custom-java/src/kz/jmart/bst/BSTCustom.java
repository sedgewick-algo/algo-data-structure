package kz.jmart.bst;

import java.util.LinkedList;
import java.util.Queue;

public class BSTCustom {
    TreeNode root;

    public TreeNode insert(TreeNode root, int val) {
        if(root == null) {
            root = new TreeNode(val);
        } else {
            if(val > root.val) {
                root.right = insert(root.right, val);
            } else {
                root.left = insert(root.left, val);
            }
        }
        return root;
    }

    public void inOrder(TreeNode root) {
        if(root != null) {
            inOrder(root.left);
            System.out.print(root.val + " ");
            inOrder(root.right);
        }
    }

    public void levelOrder(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        TreeNode head;
        while(!queue.isEmpty()) {
            head = queue.poll();
            if (head != null) {
                System.out.print(head.val + " ");
                if (head.left != null) {
                    queue.add(head.left);
                }
                if (head.right != null) {
                    queue.add(head.right);
                }
            }
        }
    }

    public void reverseOrder(TreeNode root) {
        if(root != null) {
            reverseOrder(root.right);
            System.out.print(root.val + " ");
            reverseOrder(root.left);
        }
    }

    public boolean search(TreeNode root, int val) {
        if(root != null) {
            if(root.val == val) return true;
            else if(val < root.val) return search(root.left, val);
            else return search(root.right, val);
        }
        return false;
    }

    public int height(TreeNode root) {
        int depth = depth(root);
        return depth > 0 ? depth - 1 : 0;
    }
    public int depth(TreeNode root) {
        if(root == null) return 0;

        int left = depth(root.left);
        int right = depth(root.right);

        return left > right ? 1 + left : 1 +  right;
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    public TreeNode() {}

    public TreeNode(int val) {
        this.val = val;
        this.left = this.right = null;
    }
}
